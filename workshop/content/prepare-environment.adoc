:USER_GUID: %GUID%
:USERNAME: %USERNAME%
:markup-in-source: verbatim,attributes,quotes
:show_solution: true

== Accessing your Lab Environment

Every attendee gets their own lab environment. The labs have already been deployed, to access your lab you need a unique _Identifier (GUID)_ that will be part of the hostnames and URLs you need to access.

[source,bash,options="nowrap",subs="{markup-in-source}"]
----
Here is some important information about your environment:

Your GUID is: *{USER_GUID}*

Your username is: *{USERNAME}*

----

=== Access the Satellite UI

* Open a browser to access the Satellite User Interface at:
[source,options="nowrap",subs="{markup-in-source}"]
----
https://satellite.%GUID%.blue.osp.opentlc.com
----
* Satellite uses a self-signed certificate, which means you will need to confirm a security exception in the browser to view the UI
image:assets/prepare-environment/images/summit_security_exception.png[security]
image:assets/prepare-environment/images/summit_security_confirmation.png[security]
* The username to login in the UI is `admin`
* The password is `admin`

=== Access a host via SSH

* The lab instructions will ask you to access the terminal on a certain system.
* To do this, you will have to access it through the host called the `bastion host`, which is the entrypoint to the lab environment.
* Open a terminal session to log in to your bastion host as the `lab-user`:
+
[source,bash,options="nowrap",subs="{markup-in-source}"]
----
ssh %USERNAME%@host-%GUID%.rhpds.opentlc.com
----

* An ssh key will be set up on your laptop to allow passwordless login
 ** if this is not a lab environment use `r3dh4t1!`
* From this host, known as the `bastion host`, you can access any of the virtual machines in the environment.
* *You need to go through this host first to get to the Satellite, Capsule, and Hosts.* None of the other VMs have their ssh port exposed externally.
* The bastion host has ssh configs and ssh keys copied over for ease of use
* All of these commands can be run from the bastion host to access the machines in the lab environment:
 ** To access the Satellite: `ssh satellite`
 ** To access the Capsule: `ssh capsule`
 ** To access the Skylab host registered to Satellite: `ssh skylab`
 ** To access the Aqua host registered to the Capsule: `ssh aqua`

=== Environment

* Run this to get to your bastion host:
+
[source,bash, options="nowrap",subs="{markup-in-source}"]
----
ssh %USERNAME%@host-%GUID%.rhpds.opentlc.com
----

* From there, you can get to the satellite command line:
+
[source,bash, options="nowrap",subs="{markup-in-source}"]
----
ssh satellite
----

* As well as accessing other system's terminal, such as the `skylab` host:
[source,bash, options="nowrap",subs="{markup-in-source}"]
----
ssh skylab
----

* And you can open up the Satellite UI in your web browser
[source,bash, options="nowrap",subs="{markup-in-source}"]
----
https://satellite.%GUID%.blue.osp.opentlc.com
----

* Log in with username `admin` and password `admin`

If you have questions about accessing your lab environment, please reach out to a lab administrator to help

Congratulations! This was a long phase, but you are finished with your preparation work.

You have completed the following:

* Overview ...
